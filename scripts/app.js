document.addEventListener('DOMContentLoaded', async () => {
    Cart.setcart();
    UI.loadSlider();
    UI.updateFrontCategory(document.getElementsByClassName("product-view")[0].getAttribute("category"), 0, 0);
    
})
window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }