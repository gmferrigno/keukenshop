class Cart{

    static setcart(){
        let cart = Storage.getCartLS();  
        if (cart === null){
            cart = [];
        }
        UI.updateFrontCart(cart)
    }

    static getCart(){
        let cart = Storage.getCartLS();  
        if (cart === null){ //chequea si el carrito existe
            cart = [];
        }
        return cart;
    }

    static async updatecart(id, qty, op){ // id: id del item a modificar. qty: cantidad de objetos a modificar 999 si quieres sacarlo de la lista. op: "up" o "down" añade o quita items
        let cart = this.getCart();

        let i = cart.findIndex(element => element.id === id);  //encuentra el indice del item en el carrito, de no existir devuelve -1


        if (op === 'up'){
            if (i > -1){ 
                cart[i].qty += qty;
            }
            else{ 
                let newitem = await Storage.getProduct(id); //crea un nuevo item trayendolo del storage 
                if(newitem !== undefined){
                    newitem.qty = qty;
                    cart.push(newitem);
                }
                else{console.log("no existe el objeto")}
            }
        }
        else if (op === 'down' && i > -1){
            cart[i].qty -= qty;
            if(cart[i].qty < 1){cart.splice(i, 1);}
        }else if (op === 'none'){
            return
        }
        UI.updateFrontCart(cart)
        Storage.setCartLS(cart)
    }

    
}