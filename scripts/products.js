class Products {
    async getProducts() {
        let response = await fetch('http://localhost/Keukenshop/db/articulos.json'); //¿url fija o funcion para saber el depth?
        let products = await response.json();
        products = products.map(product => {
            const id = product.codigo;
            const price = parseFloat(product.costo.replace(",", ".")).toFixed(2);
            const category = product.rubro;
            const name = product.descripcion;
            return {
                id: id, 
                category: category, 
                name: name, 
                price: price,
            }
        
        }); 
        return products;
    } 
}