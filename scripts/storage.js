class Storage {
    // there is no need to instantiate the method if is declared as static
    static setProductsLS(products) {
        localStorage.setItem('products', JSON.stringify(products)) 
    }
    static async getProduct(id) {
        let products = await Storage.getProducts();
        return products.find(Product => Product.id === id);
    }
    static setCartLS(cart) {
        localStorage.setItem('cart', JSON.stringify(cart))
    }
    static getCartLS() {
        return localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
    }
    static async getProducts(){
        let products = JSON.parse(localStorage.getItem('products'));
        if (products === null){ console.log("Datos no encontrados en localstorage"); 
            const p = new Products();
            Storage.setProductsLS(await p.getProducts());
            products = JSON.parse(localStorage.getItem('products'));
    }
    return products;
    }
}