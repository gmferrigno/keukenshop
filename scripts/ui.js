class UI{
    static updateFrontCart(cart){
        const loopDevices = (typeProducts) => { 
            let output = '';
            if(typeProducts.length === 0){
                return `carrito vacio`;
            }else{
            typeProducts.forEach(typeProduct => {
                // output variable
                output += `
                <div class="cartproduct">
                    <img src="http://localhost/Keukenshop/resources/imagentest.jpg" alt=${typeProduct.name}>
                    <div class="product-details">
                        <p class="product-name">${typeProduct.name}</p>
                        <p class="product-price">Precio por unidad: $ ${typeProduct.price}</p>
                        <div class="adjust-qty">
                                    <button class="adjust-button" onclick="Cart.updatecart(${typeProduct.id}, 1, 'down')">-</button> <span class="adjust-nubr"> ${typeProduct.qty} </span>  <button class="adjust-button" onclick="Cart.updatecart(${typeProduct.id}, 1, 'up')">+</button>
                                </div>
                        <p class="product-total">llevas ${typeProduct.qty} por $${(typeProduct.price * typeProduct.qty).toFixed(2)} </p>
                    </div>
                </div>
                `;
            }); 
        return output; 
        }}
        let cartoutput = `<div id="close-cart" onclick="UI.cartButton()"><i class="fa fa-times-circle" aria-hidden="true"></i></div> <a href="http://localhost/Keukenshop/pages/checkout.html"><button class="add-cart">Proceder al pago</button></a>`;
        cartoutput += loopDevices(cart); 
        let total = 0;
        cart.forEach(element => total += element.price * element.qty);
        cartoutput+= `<p id="cart-total-p">Total: $<span id="carttotal">${total.toFixed(2)}</span></p>`;

        document.getElementById('cart-view').innerHTML = cartoutput;
        document.getElementById("cartqty").innerHTML = cart.length;
            
    }

    static cartButton(){
        let cartview = document.getElementById('cart-view');    
        if(cartview.style.display === "block"){
            cartview.style.display = "none";
            document.getElementById('cart-overlay').style.display = "none";
       } else{
        cartview.style.display = "block";
        document.getElementById('cart-overlay').style.display = "block";
       }
    }

    static async updateFrontCategory(category, index, viewerN){
        if(category === "all"){
            console.log("all"); //hacer metodo para mostrar todos los items
        } else{
            let products = await Storage.getProducts();
            let categoryproducts = products.filter(product => product.category === category);
            let indexedproducts;
            let products_Output
            if (categoryproducts.length === 0){
                products_Output = `<div class="alert">
                                        <p>no se han encontrado resultados para la categoria: ${category}</p>
                                        </div>`
            } else{

                let itemsPerPage;
                let screenw = screen.width;
                if (screenw > 420){
                    itemsPerPage = 8;
                }else{
                    itemsPerPage = 4;
                }


                indexedproducts = categoryproducts.slice(index, index + itemsPerPage);
                
                const loopDevices = (typeProducts) => { 
                    let output = '';
                    let i = 0;
                    let cart = Cart.getCart(); 
                    typeProducts.forEach(typeProduct => { 
                
                        // output variable
                        let item = cart.find(element => element.id === typeProduct.id);
                        output += `
                        <div class="product">
                            <a href="http://localhost/Keukenshop/pages/product%20view.html?id=${typeProduct.id}">
                            <img src="http://localhost/Keukenshop/resources/imagentest.jpg" alt=${typeProduct.name}> </a>
                            <div class="product-details">
                                <p class="product-name">${typeProduct.name}</p>
                                <p class="product-price">$ ${typeProduct.price}</p>
                                                    
                                <button class="add-cart" id="btn${i}" onclick="UI.addmenu(${typeProduct.id},'up', ${i})" `; if(item !== undefined){output += `style="display: none;"`;}; output += `  >
                                <i class="fa fa-arrow-right" aria-hidden="true"></i> <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                </button>

                                <div class="adjust-qty" id="adj${i}" `; if(item === undefined){output += `style="display: none;"`;}; output += `>
                                    <button class="adjust-button" onclick="UI.addmenu(${typeProduct.id},'down', ${i})">-</button> <span class="adjust-nubr" id="adjn${i}"> `; if(item !== undefined){output += item.qty;}; output += ` </span>  <button class="adjust-button" onclick="UI.addmenu(${typeProduct.id},'up', ${i})">+</button>
                                </div>
                                
                            </div>
                        </div>
                        `;
                        i++;
                    });
                return output; 
                }
                products_Output = `<h2 style="font-size: 1.5rem; text-align: left; margin-left: 7%;">${category}:</h2>`;
                // returning values to the output variables using the function    
                
                if (index === 0){ // si index =0 no mostrar boton anterior
                    products_Output += `<div class="nexticon" onclick="UI.updateFrontCategory('${category}', ${index+itemsPerPage}, ${viewerN})"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>`
                } else if(categoryproducts.slice(index + itemsPerPage, index + itemsPerPage*2).length === 0){// si en el corte siguiente no hay items no montrar boton siguiente
                    products_Output += `<div class="previcon" onclick="UI.updateFrontCategory('${category}', ${index-itemsPerPage}, ${viewerN})"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </div>`

                } else{ //poner botones antes y despues, con funcion que llame a esto con un index 10 menos y 10 mas
                    products_Output += `<div class="previcon" onclick="UI.updateFrontCategory('${category}', ${index-itemsPerPage}, ${viewerN})"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </div> <div class="nexticon" onclick="UI.updateFrontCategory('${category}', ${index+itemsPerPage}, ${viewerN})"><i class="fa fa-arrow-right" aria-hidden="true"></i></div>`
                }
                products_Output += '<div>'; products_Output += loopDevices(indexedproducts); products_Output += '</div>';
            }
            // dynamically updating the DOM with output variables values
            document.getElementsByClassName('product-view')[viewerN].innerHTML = products_Output;
   
        }
    }

    static async addmenu(id, op, cid){
        await Cart.updatecart(id, 1, op);
        let cart = Cart.getCart();
        if (cart.find(element => element.id === id)){
            document.getElementById("btn" + cid).style.display = "none";
            document.getElementById("adj" + cid).style.display = "block";
            document.getElementById("adjn" + cid).innerHTML = cart.find(element => element.id === id).qty;
        } else{
            document.getElementById("btn" + cid).style.display = "inline-block";
            document.getElementById("adj" + cid).style.display = "none";
        }
    }

     static async searchbar(){
        let searched = document.getElementById("searchbox").value.toUpperCase();
        if(searched.length > 2){
            let products = await Storage.getProducts();
            let i = 0;
            let searchresults = products.filter(product => { 
                if (product.name.toUpperCase().includes(searched) && i < 10){
                    i++;
                    return true;
                } else return false;
            
            });
            if (i < 10){
                searchresults = searchresults.concat(
                    products.filter(product => { 
                        if ( product.category.toUpperCase().includes(searched) && i < 10){
                            i++;
                            return true;
                        } else return false;
                    })
                );
            }

            const loopDevices = (typeProducts) => { 
                let output = '';
                if(typeProducts.length === 0){
                    return `No hay resultados`;
                }else{
                typeProducts.forEach(typeProduct => {
                
                    output += `
                    <a href="http://localhost/Keukenshop/pages/product%20view.html?id=${typeProduct.id}"><li><img src="http://localhost/Keukenshop/resources/imagentest.jpg" href="#" alt=""> <p>${typeProduct.name}</p> <p>Precio: $ ${typeProduct.price}</p> </li> </a>
                    `;
                });
            return output; 
            }}
            document.getElementById("search-results").innerHTML = loopDevices(searchresults);
        }

        
    }

    static openSearch(){
        document.getElementById("search-overlay").style.display = "block";
        document.getElementById("search-modal").style.display = "block";
        document.getElementById("searchbox").focus();
    }

    static hidesearch(){ 
        document.getElementById("search-overlay").style.display = "none";
        document.getElementById("search-modal").style.display = "none";
   }

    static async loadProductView(){
        
        let searchParams = new URLSearchParams(window.location.search);
        if (!(searchParams.has("id"))){
            document.getElementsByClassName('product-view')[0].innerHTML = `
            <p>has llegado aqui por mal camino</p>           
            `;
        }else{
            
            let Product = await Storage.getProduct(parseInt(searchParams.get("id"), 10));
            let item = Cart.getCart().find(element => element.id === Product.id);
                
            let output = `

                <p class="category-description">${Product.category}</p>
                <h1 class="category-title">${Product.name}</h1>
                <img class="category-image" src="./../resources/imagentest.jpg" alt="">
                <div class="add-menu">
                    <p class="category-price">$ ${Product.price}</p> 
                    <button class="add-cart" id="btn1" onclick="UI.addmenu(${Product.id},'up', 1)" `; if(item !== undefined){output += `style="display: none;"`;}; output += `  >
                        Add to Cart <span><i class="lni lni-cart"></i></span>
                    </button>
                    <div class="adjust-qty" id="adj1" `; if(item === undefined){output += `style="display: none;"`;}; output += `>
                    <button class="adjust-button" onclick="UI.addmenu(${Product.id},'down', 1)">-</button> <span class="adjust-nubr" id="adjn1"> `; if(item !== undefined){output += item.qty;}; output += ` </span>  <button class="adjust-button" onclick="UI.addmenu(${Product.id},'up', 1)">+</button>
                    </div>
                </div>          
            `;
            document.getElementsByClassName('product-view')[0].innerHTML = output;

        }
    }

    static loadSlider(){
        let output = `
        <img class="slideimg" src="./resources/slider1.jpg" alt="" style="display:block;">
        <img class="slideimg" src="./resources/slider2.jpg" alt="" style="display: none;">
        <img class="slideimg" src="./resources/slider3.jpg" alt="" style="display: none;">

    
        <div class="slidernav">
            <div class="slidebutton prev" onclick="UI.sliderbutton(-1)">❮</div>
            <div class="slidebutton next" onclick="UI.sliderbutton(1)">❯</div>
            <span class="slidercircle cActive" onclick="UI.sliderselect(0)"></span>
            <span class="slidercircle" onclick="UI.sliderselect(1)"></span>
            <span class="slidercircle" onclick="UI.sliderselect(2)"></span>
    
        </div>
        `;
        document.getElementById("slider").innerHTML = output;
    }

    static sliderbutton(n){
        let slides = document.getElementsByClassName("slideimg");
        let actual = parseInt(document.getElementById("slider").getAttribute("slide")) + n;
        if (actual > slides.length - 1){actual = 0}
        else if(actual < 0){actual = slides.length - 1;}
        UI.sliderselect(actual);
    }   

    static sliderselect(n){
        let slides = document.getElementsByClassName("slideimg");
        let circles = document.getElementsByClassName("slidercircle"); 
            for (let i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            slides[n].style.display = "block";
            for (let i = 0; i < circles.length; i++) {
                circles[i].classList.remove("cActive");
              }
              circles[n].className += " cActive"
              document.getElementById("slider").setAttribute("slide", n);
    }
    
    static loadCheckout(){
        let cart = Cart.getCart();  
        const loopDevices = (typeProducts) => { 
            let output = '';
            if(typeProducts.length === 0){
                return `carrito vacio`;
            }else{
            typeProducts.forEach(typeProduct => {
                // output variable
                output += `
                <div class="cartproduct">
                    <img src="http://localhost/Keukenshop/resources/imagentest.jpg" alt=${typeProduct.name}>
                    <div class="product-details">
                        <p class="product-name">${typeProduct.name}</p>
                        <p class="product-price">Precio por unidad: $ ${typeProduct.price}</p> 
                        <div class="adjust-qty">
                                    <button class="adjust-button" onclick="Cart.updatecart(${typeProduct.id}, 1, 'down'); UI.loadCheckout()">-</button> <span class="adjust-nubr"> ${typeProduct.qty} </span>  <button class="adjust-button" onclick="Cart.updatecart(${typeProduct.id}, 1, 'up'); UI.loadCheckout()">+</button>
                                </div>
                            <p class="product-total">llevas ${typeProduct.qty} por $${(typeProduct.price * typeProduct.qty).toFixed(2)} </p>
                        </div>
                </div>
                `;
            }); 
        return output; 
        }}
        let cartoutput = loopDevices(cart); 
        let total = 0;
        cart.forEach(element => total += element.price * element.qty);
        cartoutput+= `<p id="cart-total-p">Total: $<span id="carttotal">${total.toFixed(2)}</span></p>`;
        cartoutput+= '<button class="pagbutton" id="paymentbutton">Proceder al pago</button>'
        
        document.getElementById('checkout-view').innerHTML = cartoutput;

    }
    static showcategories(){
        document.getElementById("myDropdown").classList.toggle("show");
    }

    static openmenu(){
        document.getElementById("mobile-menu").style.display = "block";
        document.getElementById("cartbutton").style.display = "none";
    }

    static closemenu(){
        document.getElementById("mobile-menu").style.display = "none";
        document.getElementById("cartbutton").style.display = "inline-block";
    }
}